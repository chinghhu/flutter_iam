# fluttertest

測試firebase In-App Messaging [IAM]. 

相比起來IAM, Remote Config 是傳送一組server 和 APP間設計好的資料集, 與UI 和Messaging 無直接關係. 所以應該不適用你的case. 我就沒去試用.


## IAM 限制

1. IAM event (custom events and standard events). 最多每日發送一次.
可以在firebase console 使用`Test on device` 允許重複發送events.

2. 但 `Test on device` 有其限制. 只有app 第一次resume 到foreground 時, event 才會take effect. see [Issue with Test on device]. 
因此, 與其使用`Test on device`, 不如每次測試時, 使用一個新的VM反而好用.

3. IAM Widget 只能是獨立的popup widget. 無法內嵌於app UI 內.

4. IAM Widget 內容是在firebase console 設定完成, app 無法改變.

5. IAM 貌似只支援Android 與 iOS. see [Flutter IAM].

## IAM UI

Message的格式有 `Card`, `Modal`, `Image Only`, and `Banner`. 有的可以加入action button. action button 按下後要處理甚麼, 要看你的應用.

目前看起來是無法把IAM widget內嵌到app UI裡.

## Standard event

Standard events 是像`to_foreground`, `app on`, `session_start`這類跟app lifecycle 相關時機的事件.

Standard event 應該也不適用你個case. 尤其是要trigger in-app messages programmatically. 

## Custom event

可以自行定義event (e.g. 'myEvent'). 在app 裡用`FirebaseInAppMessaging.triggerEvent('myEvent');` 觸發event, 通知server 發送message.

但仍受限於頻率限制, server一天最多發送一次message. 例如, 在sample code中, 只有第一次按`PROGRAMMATIC TRIGGERS`時, 才會出現IAM.


[IAM]: https://firebase.google.com/docs/in-app-messaging/get-started?authuser=0&platform=android
[Issue with Test on device]: https://www.harddone.com/post/FIAM_triggerEvent/
[Flutter IAM]: https://pub.dev/packages/firebase_in_app_messaging/versions